package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.ua.nure.Flowers;
import org.xml.sax.SAXException;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.List;

public interface FlowersController {
    String XSD_VALIDATOR = "input.xsd";

    List<Flowers.Flower> getFlowers() throws IOException, SAXException, XMLStreamException;
}
