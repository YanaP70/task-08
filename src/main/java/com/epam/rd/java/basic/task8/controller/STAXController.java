package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.ua.nure.Flowers;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler implements FlowersController {

    private String xmlFileName;
    private Deque<Flowers.Flower> flowers;

    private XMLEventReader reader;
    private String currentElement;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;

        XMLInputFactory factory = XMLInputFactory.newInstance();
        try {
            validateXMLSchema();

            reader = factory.createXMLEventReader(new StreamSource(xmlFileName));
        } catch (XMLStreamException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Flowers.Flower> getFlowers() throws XMLStreamException {
        buildListFlowers();
        return new ArrayList<>(flowers);
    }

    private void buildListFlowers() throws XMLStreamException {
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event == null || event.isCharacters() && event.asCharacters().isWhiteSpace()) {
                continue;
            }
            if (event.isStartDocument()) {
                flowers = new ArrayDeque<>();
                continue;
            }
            if (event.isStartElement()) {
                StartElement startElement = event.asStartElement();
                currentElement = startElement.getName().getLocalPart();

                if (currentElement.equals("flower")) {
                    flowers.offer(new Flowers.Flower());
                }
                if (!flowers.isEmpty()) {
                    XMLParseUtil.startElement(currentElement, flowers.peekLast(), createMapAttributes(startElement));
                }
                continue;
            }
            if (event.isEndElement()) {
                currentElement = "";
                continue;
            }
            if (event.isCharacters() && !flowers.isEmpty()) {
                XMLParseUtil.characters(currentElement, flowers.peekLast(), event.asCharacters().getData());
            }
        }
    }

    private void validateXMLSchema() throws SAXException, IOException {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = factory.newSchema(new File(XSD_VALIDATOR));
        Validator validator = schema.newValidator();

        validator.validate(new StreamSource(new File(xmlFileName)));
    }

    private Map<String, String> createMapAttributes(StartElement startElement) {
        Map<String, String> map = new HashMap<>();
        Iterator<Attribute> iterator = startElement.getAttributes();
        while (iterator.hasNext()) {
            Attribute attribute = iterator.next();
            String name = attribute.getName().getLocalPart();
            if (name.equals("measure") || name.equals("lightRequiring")) {
                map.put(name, attribute.getValue());
            }
        }
        return map;
    }
}