package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.ua.nure.Flowers;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler implements FlowersController {

    private final String xmlFileName;
    private Deque<Flowers.Flower> flowers;
    private XMLReader xmlReader;

    private String nameOfCurrentElement;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;

        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setNamespaceAware(true);
            SAXParser parser = factory.newSAXParser();
            xmlReader = parser.getXMLReader();
            xmlReader.setContentHandler(this);
        } catch (SAXException | ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Flowers.Flower> getFlowers() throws IOException, SAXException {
        buildListFlowers();
        return new ArrayList<>(flowers);
    }

    private void buildListFlowers() throws IOException, SAXException {
        validateXMLSchema();
        xmlReader.parse(xmlFileName);
    }

    private void validateXMLSchema() throws SAXException, IOException {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = factory.newSchema(new File(XSD_VALIDATOR));
        Validator validator = schema.newValidator();

        validator.validate(new StreamSource(new File(xmlFileName)));
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();

        flowers = new ArrayDeque<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);

        if (qName.equals("flower")) {
            flowers.offer(new Flowers.Flower());
        }
        nameOfCurrentElement = qName;
        Flowers.Flower flower = flowers.peekLast();

        Map<String, String> map = new HashMap<>();
        map.put("measure", attributes.getValue("measure"));
        map.put("lightRequiring", attributes.getValue("lightRequiring"));

        if (flower != null) {
            XMLParseUtil.startElement(nameOfCurrentElement, flower, map);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);

        nameOfCurrentElement = "";
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);

        String value = String.valueOf(ch).substring(start, start + length);
        Flowers.Flower flower = flowers.peekLast();

        if (flower != null) {
            XMLParseUtil.characters(nameOfCurrentElement, flower, value);
        }
    }
}
