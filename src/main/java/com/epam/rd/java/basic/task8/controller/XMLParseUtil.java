package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.ua.nure.Flowers;

import java.math.BigInteger;
import java.util.Map;

public class XMLParseUtil {
    public static void startElement(String nameOfCurrentElement, Flowers.Flower flower, Map<String, String> map) {
        switch (nameOfCurrentElement) {
            case "visualParameters":
                flower.setVisualParameters(new Flowers.Flower.VisualParameters());
                break;
            case "growingTips":
                flower.setGrowingTips(new Flowers.Flower.GrowingTips());
                break;
            case "aveLenFlower":
                flower.getVisualParameters().setAveLenFlower(new Flowers.Flower.VisualParameters.AveLenFlower());
                flower.getVisualParameters().getAveLenFlower().setMeasure(map.get("measure"));
                break;
            case "lighting":
                flower.getGrowingTips().setLighting(new Flowers.Flower.GrowingTips.Lighting());
                flower.getGrowingTips().getLighting().setLightRequiring(map.get("lightRequiring"));
                break;
            case "tempreture":
                flower.getGrowingTips().setTempreture(new Flowers.Flower.GrowingTips.Tempreture());
                flower.getGrowingTips().getTempreture().setMeasure(map.get("measure"));
                break;
            case "watering":
                flower.getGrowingTips().setWatering(new Flowers.Flower.GrowingTips.Watering());
                flower.getGrowingTips().getWatering().setMeasure(map.get("measure"));
                break;
        }
    }

    public static void characters(String nameOfCurrentElement, Flowers.Flower flower, String value) {

        switch (nameOfCurrentElement) {
            case "name":
                flower.setName(value);
                break;
            case "soil":
                flower.setSoil(value);
                break;
            case "origin":
                flower.setOrigin(value);
                break;
            case "multiplying":
                flower.setMultiplying(value);
                break;
            case "stemColour":
                flower.getVisualParameters().setStemColour(value);
                break;
            case "leafColour":
                flower.getVisualParameters().setLeafColour(value);
                break;
            case "aveLenFlower":
                flower.getVisualParameters().getAveLenFlower().setValue(new BigInteger(value));
                break;
            case "tempreture":
                flower.getGrowingTips().getTempreture().setValue(new BigInteger(value));
                break;
            case "watering":
                flower.getGrowingTips().getWatering().setValue(new BigInteger(value));
                break;
        }
    }
}
