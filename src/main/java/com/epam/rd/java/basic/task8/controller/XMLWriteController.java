package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.ua.nure.Flowers;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class XMLWriteController {
    private static final String XSD_URL = "http://www.nure.ua";

    private static DocumentBuilder docBuilder;

    static {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public static void writeListFlowers(String xmlOutFileName, List<Flowers.Flower> list) {
        Document document = docBuilder.newDocument();
        String root = "flowers";
        Element rootElement = document.createElementNS(XSD_URL, root);
        document.appendChild(rootElement);

        for (Flowers.Flower flower : list) {
            Element elementFlower = createFlowerElement(document, flower);
            rootElement.appendChild(elementFlower);
        }
        writeTreeToFile(document, xmlOutFileName);
    }

    private static Element createFlowerElement(Document document, Flowers.Flower flower) {
        Element elementFlower = document.createElement("flower");

        Element element = document.createElement("name");
        element.appendChild(document.createTextNode(flower.getName()));
        elementFlower.appendChild(element);

        element = document.createElement("soil");
        element.appendChild(document.createTextNode(flower.getSoil()));
        elementFlower.appendChild(element);

        element = document.createElement("origin");
        element.appendChild(document.createTextNode(flower.getOrigin()));
        elementFlower.appendChild(element);

        element = createVisualParametersElement(document, flower);
        elementFlower.appendChild(element);

        element = createGrowingTipsElement(document, flower);
        elementFlower.appendChild(element);

        element = document.createElement("multiplying");
        element.appendChild(document.createTextNode(flower.getMultiplying()));
        elementFlower.appendChild(element);

        return elementFlower;
    }

    private static Element createVisualParametersElement(Document document, Flowers.Flower flower) {
        Element elementVP = document.createElement("visualParameters");

        Element element = document.createElement("stemColour");
        element.appendChild(document.createTextNode(flower.getVisualParameters().getStemColour()));
        elementVP.appendChild(element);

        element = document.createElement("leafColour");
        element.appendChild(document.createTextNode(flower.getVisualParameters().getLeafColour()));
        elementVP.appendChild(element);

        if (flower.getVisualParameters().getAveLenFlower() != null && flower.getVisualParameters().getAveLenFlower().getValue() != null) {
            element = document.createElement("aveLenFlower");
            element.appendChild(document.createTextNode(flower.getVisualParameters().getAveLenFlower().getValue().toString()));
            elementVP.appendChild(element);
            element.setAttribute("measure", flower.getVisualParameters().getAveLenFlower().getMeasure());
        }
        return elementVP;
    }

    private static Element createGrowingTipsElement(Document document, Flowers.Flower flower) {
        Element elementGrowing = document.createElement("growingTips");

        Element element = document.createElement("tempreture");
        element.appendChild(document.createTextNode(flower.getGrowingTips().getTempreture().getValue().toString()));
        elementGrowing.appendChild(element);
        element.setAttribute("measure", flower.getGrowingTips().getTempreture().getMeasure());

        element = document.createElement("lighting");
        elementGrowing.appendChild(element);
        element.setAttribute("lightRequiring", flower.getGrowingTips().getLighting().getLightRequiring());

        element = document.createElement("watering");
        element.appendChild(document.createTextNode(flower.getGrowingTips().getWatering().getValue().toString()));
        elementGrowing.appendChild(element);
        element.setAttribute("measure", flower.getGrowingTips().getWatering().getMeasure());

        return elementGrowing;
    }

    private static void writeTreeToFile(Document document, String xmlFileName) {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new FileWriter(xmlFileName));
            transformer.transform(source, result);
        } catch (TransformerException | IOException e) {
            e.printStackTrace();
        }
    }

}
