package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.DOMController;
import com.epam.rd.java.basic.task8.controller.SAXController;
import com.epam.rd.java.basic.task8.controller.STAXController;
import com.epam.rd.java.basic.task8.controller.XMLWriteController;
import com.epam.rd.java.basic.task8.ua.nure.Flowers;

import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        // PLACE YOUR CODE HERE
        List<Flowers.Flower> listFlowersDOM = new DOMController(xmlFileName).getFlowers();

        // sort (case 1)
        // PLACE YOUR CODE HERE
        listFlowersDOM.sort(Flowers.cmpFlowerByOriginName);

        // save
        String outputXmlFile = "output.dom.xml";
        // PLACE YOUR CODE HERE
        XMLWriteController.writeListFlowers(outputXmlFile, listFlowersDOM);

        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        // get
        // PLACE YOUR CODE HERE
        List<Flowers.Flower> listFlowersSAX = new SAXController(xmlFileName).getFlowers();

        // sort  (case 2)
        // PLACE YOUR CODE HERE
        listFlowersSAX.sort(Flowers.cmpFlowerByName);

        // save
        outputXmlFile = "output.sax.xml";
        // PLACE YOUR CODE HERE
        XMLWriteController.writeListFlowers(outputXmlFile, listFlowersSAX);

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        // get
        // PLACE YOUR CODE HERE
        List<Flowers.Flower> listFlowersSTAX = new STAXController(xmlFileName).getFlowers();

        // sort  (case 3)
        // PLACE YOUR CODE HERE
        listFlowersSTAX.sort(Flowers.cmpFlowerByWatering);

        // save
        outputXmlFile = "output.stax.xml";
        // PLACE YOUR CODE HERE
        XMLWriteController.writeListFlowers(outputXmlFile, listFlowersSTAX);
    }

}
