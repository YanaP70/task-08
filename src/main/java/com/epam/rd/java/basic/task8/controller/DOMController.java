package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.ua.nure.Flowers;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController implements FlowersController {
    private final String xmlFileName;
    private List<Flowers.Flower> flowers;

    private DocumentBuilder docBuilder;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
        this.flowers = new ArrayList<>();

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            docBuilder = factory.newDocumentBuilder();

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Flowers.Flower> getFlowers() throws IOException, SAXException {
        buildListFlowers();
        return new ArrayList<>(flowers);
    }

    private void buildListFlowers() throws IOException, SAXException {
        flowers = new ArrayList<>();

        Document doc;
        doc = docBuilder.parse(xmlFileName);
        Element root = doc.getDocumentElement();

        validateXMLSchema(root);

        NodeList flowerList = root.getElementsByTagName("flower");

        for (int i = 0; i < flowerList.getLength(); i++) {
            Element flowerElement = (Element) flowerList.item(i);
            Flowers.Flower flower = buildFlower(flowerElement);
            flowers.add(flower);
        }
    }

    private void validateXMLSchema(Element rootElement) throws SAXException, IOException {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = factory.newSchema(new File(XSD_VALIDATOR));
        Validator validator = schema.newValidator();

        validator.validate(new DOMSource(rootElement));
    }

    private Flowers.Flower buildFlower(Element flowerElement) {
        Flowers.Flower flower = new Flowers.Flower();

        flower.setName(getElementTextContent(flowerElement, "name"));
        flower.setSoil(getElementTextContent(flowerElement, "soil"));
        flower.setOrigin(getElementTextContent(flowerElement, "origin"));
        flower.setMultiplying(getElementTextContent(flowerElement, "multiplying"));

        Element visualParametersElement = (Element) flowerElement.getElementsByTagName("visualParameters").item(0);
        if (visualParametersElement != null) {
            Flowers.Flower.VisualParameters visualParameters = buildVisualParameters(visualParametersElement);
            flower.setVisualParameters(visualParameters);
        }

        Element growingTipsElement = (Element) flowerElement.getElementsByTagName("growingTips").item(0);
        if (growingTipsElement != null) {
            Flowers.Flower.GrowingTips growingTips = buildGrowingTipsElement(growingTipsElement);
            flower.setGrowingTips(growingTips);
        }
        return flower;
    }

    private Flowers.Flower.GrowingTips buildGrowingTipsElement(Element element) {
        Flowers.Flower.GrowingTips growingTips = new Flowers.Flower.GrowingTips();

        growingTips.setLighting(buildLighting(element));

        Flowers.Flower.GrowingTips.Tempreture tempreture = new Flowers.Flower.GrowingTips.Tempreture();
        buildTemplate(element, "tempreture", tempreture);
        growingTips.setTempreture(tempreture);

        Flowers.Flower.GrowingTips.Watering watering = new Flowers.Flower.GrowingTips.Watering();
        buildTemplate(element, "watering", watering);
        growingTips.setWatering(watering);

        return growingTips;
    }


    private Flowers.Flower.VisualParameters buildVisualParameters(Element element) {
        Flowers.Flower.VisualParameters visualParameters = new Flowers.Flower.VisualParameters();
        visualParameters.setStemColour(getElementTextContent(element, "stemColour"));
        visualParameters.setLeafColour(getElementTextContent(element, "leafColour"));

        NodeList nodeList = element.getElementsByTagName("aveLenFlower");
        if (nodeList != null && nodeList.getLength() == 1) {
            Flowers.Flower.VisualParameters.AveLenFlower aveLenFlower = new Flowers.Flower.VisualParameters.AveLenFlower();
            buildTemplate(nodeList, aveLenFlower);
            visualParameters.setAveLenFlower(aveLenFlower);
        }

        return visualParameters;
    }

    private Flowers.Flower.GrowingTips.Lighting buildLighting(Element element) {
        Flowers.Flower.GrowingTips.Lighting lighting = new Flowers.Flower.GrowingTips.Lighting();

        Node lightingNode = element.getElementsByTagName("lighting").item(0);
        if (lightingNode != null && lightingNode.getAttributes() != null) {
            Node attrNode = lightingNode.getAttributes().getNamedItem("lightRequiring");

            if (attrNode != null) {
                lighting.setLightRequiring(attrNode.getTextContent());
            }
        }
        return lighting;
    }

    private void buildTemplate(NodeList nodeList, Flowers.Flower.Template template) {
        buildTemplateNode(nodeList, template);
    }

    private void buildTemplate(Element element, String name, Flowers.Flower.Template template) {
        NodeList nodeList = element.getElementsByTagName(name);

        if (nodeList != null && nodeList.getLength() == 1) {
            buildTemplateNode(nodeList, template);
        }
    }

    private void buildTemplateNode(NodeList nodeList, Flowers.Flower.Template template) {
        Node templNode = nodeList.item(0);

        String value = templNode.getTextContent();
        template.setValue(new BigInteger(value));

        if (templNode.getAttributes() != null) {
            Node attrNode = templNode.getAttributes().getNamedItem("measure");

            if (attrNode != null) {
                template.setMeasure(attrNode.getTextContent());
            }
        }
    }

    private static String getElementTextContent(Element element, String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        if (nList == null || nList.getLength() != 1) {
            return null;
        }
        return nList.item(0).getTextContent();
    }
}
