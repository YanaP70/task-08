package com.epam.rd.java.basic.task8.ua.nure;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Flowers {
    protected List<Flowers.Flower> flower;

    public List<Flowers.Flower> getFlower() {
        if (flower == null) {
            flower = new ArrayList<>();
        }
        return this.flower;
    }

    public static Comparator<Flowers.Flower> cmpFlowerByName = Comparator.comparing(Flower::getName);
    public static Comparator<Flowers.Flower> cmpFlowerByOriginName = Comparator.comparing(Flower::getOrigin).thenComparing(Flower::getName);
    public static Comparator<Flowers.Flower> cmpFlowerByWatering = Comparator.comparing(o -> o.getGrowingTips().getWatering().getMeasure());

    public static class Flower {
        protected String name;
        protected String soil;
        protected String origin;
        protected VisualParameters visualParameters;
        protected GrowingTips growingTips;
        protected String multiplying;

        public String getName() {
            return name;
        }

        public void setName(String value) {
            this.name = value;
        }

        public String getSoil() {
            return soil;
        }

        public void setSoil(String value) {
            this.soil = value;
        }

        public String getOrigin() {
            return origin;
        }

        public void setOrigin(String value) {
            this.origin = value;
        }

        public VisualParameters getVisualParameters() {
            return visualParameters;
        }

        public void setVisualParameters(VisualParameters value) {
            this.visualParameters = value;
        }

        public GrowingTips getGrowingTips() {
            return growingTips;
        }

        public void setGrowingTips(GrowingTips value) {
            this.growingTips = value;
        }

        public String getMultiplying() {
            return multiplying;
        }

        public void setMultiplying(String value) {
            this.multiplying = value;
        }

        @Override
        public String toString() {
            return "\nFlower{" +
                    "\n  name='" + name + '\'' +
                    ",\n  soil='" + soil + '\'' +
                    ",\n  origin='" + origin + '\'' +
                    ",\n  visualParameters=" + visualParameters +
                    ",\n  growingTips=" + growingTips +
                    ",\n  multiplying='" + multiplying + '\'' +
                    "\n";
        }

        public static class GrowingTips {
            protected Tempreture tempreture;
            protected Lighting lighting;
            protected Watering watering;

            public Tempreture getTempreture() {
                return tempreture;
            }

            public void setTempreture(Tempreture value) {
                this.tempreture = value;
            }

            public Lighting getLighting() {
                return lighting;
            }

            public void setLighting(Lighting value) {
                this.lighting = value;
            }

            public Watering getWatering() {
                return watering;
            }

            public void setWatering(Watering value) {
                this.watering = value;
            }


            public static class Lighting {
                protected String lightRequiring;

                public String getLightRequiring() {
                    return lightRequiring;
                }

                public void setLightRequiring(String value) {
                    this.lightRequiring = value;
                }

                @Override
                public String toString() {
                    return "Lighting{" +
                            "lightRequiring='" + lightRequiring + '\'' +
                            '}';
                }
            }

            public static class Tempreture extends Template {
                @Override
                public String toString() {
                    return "Tempreture" + super.toString();
                }
            }

            public static class Watering extends Template {
                @Override
                public String toString() {
                    return "Watering" + super.toString();
                }
            }

            @Override
            public String toString() {
                return "GrowingTips{" +
                        "tempreture=" + tempreture +
                        ", lighting=" + lighting +
                        ", watering=" + watering +
                        '}';
            }
        }

        public static class VisualParameters {
            protected String stemColour;
            protected String leafColour;
            protected AveLenFlower aveLenFlower;

            public String getStemColour() {
                return stemColour;
            }

            public void setStemColour(String value) {
                this.stemColour = value;
            }

            public String getLeafColour() {
                return leafColour;
            }

            public void setLeafColour(String value) {
                this.leafColour = value;
            }

            public AveLenFlower getAveLenFlower() {
                return aveLenFlower;
            }

            public void setAveLenFlower(AveLenFlower value) {
                this.aveLenFlower = value;
            }

            public static class AveLenFlower extends Template {
                @Override
                public String toString() {
                    return "AveLenFlower" + super.toString();
                }
            }

            @Override
            public String toString() {
                return "VisualParameters" +
                        "stemColour='" + stemColour + '\'' +
                        ", leafColour='" + leafColour + '\'' +
                        ", aveLenFlower=" + aveLenFlower +
                        '}';
            }
        }

        public static class Template {
            protected BigInteger value;
            protected String measure;

            public BigInteger getValue() {
                return value;
            }

            public void setValue(BigInteger value) {
                this.value = value;
            }

            public String getMeasure() {
                return Objects.requireNonNullElse(measure, "cm");
            }

            public void setMeasure(String value) {
                this.measure = value;
            }

            @Override
            public String toString() {
                return "{" +
                        "value=" + value +
                        ", measure='" + measure + '\'' +
                        '}';
            }

        }
    }

}